<h1>SSH</h1>
<p>This guide will get you connected to your slot via SSH (Secure Shell). The article is built on assumption that you can find your slot details from the <a href="../getting_started/web_manager">Feral web manager</a>.</p>
<p>There are a lot of different operations that can be performed via SSH and many pieces of software are installed via it. It is a pretty important tool to have and be able to use for a lot of people.</p>

<h2>General information</h2>
<p>You can find the server/hostname, username and password for SSH on by clicking on the <samp>Software</samp> link to the left-hand side of the Feral web manager.</p>
<p>When trying to log in for the first time (except on KiTTY), you will receive a message which begins:</p>
<p><samp>The authenticity of host '<var>server</var>.feralhosting.com (<var>IP</var>)' can't be established.</samp></p>
<p>Naturally, in your specific case, <var>server</var> will be replaced with the name of the server you are trying to connect to and <var>IP</var> will be replaced by its IP. It will then ask you <samp>Are you sure you want to continue connecting (yes/no)?</samp> and wait for your response. After checking the details are as expected, type <kbd>yes</kbd> and press <kbd>enter</kbd> to save the host key.</p>
<p>The Windows utility KiTTY will display a different pop-up and that will be covered in the Windows section.</p>
<p>When you are asked to enter your password, it is important to know that your password entry will <em>not</em> appear to respond to your input - it will not display <samp>*****</samp> or something similar as you type.</p>

<h2 id="windows">Windows</h2>
<p>We will use a piece of software called KiTTY to connect to our slot via SSH. It is a fork of PuTTY with a few more useful features - it is possible to rename <i>kitty.exe</i> to <i>putty.exe</i> if other pieces of software (such as BitKinex or WinSCP) expect that filename. The first thing to do is <a href="https://www.fosshub.com/KiTTY.html">download KiTTY</a>.</p>
<p><kbd>Double-clicking</kbd> on the executable will open a configuration window. We only need to make a couple of changes to get connected.</p>
<p>In the list a categories in the left-hand pane, look for <samp>Window</samp> and click on <samp>Translation</samp>. Make sure that <kbd>UTF-8</kbd> is selected under <samp>Remote Character Set</samp>.</p>
<figure>
    <img alt="Kitty screenshot showing the location of the Translation settings and the remote character set" src="https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/slots/ssh/ssh-translation.png">
    <figcaption>The location of the Translation settings and Remote character set choices.</figcaption>
</figure>

<p>Next, look for <samp>Connection</samp> in the category list and click on <samp>Data</samp>. Make the following changes to the listed fields:</p>
<dl>
	<dt>Auto-login username</dt>
	<dd>Your SSH username</dd>
	<dt>Auto-login password</dt>
	<dd>Your SSH / SFTP / FTP password (it's the same password)</dd>
</dl>
<figure>
    <img alt="Kitty screenshot showing how to configure automatic username and password settings" src="https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/slots/ssh/ssh-autoinfo.png">
    <figcaption>The location of the Auto-login settings.</figcaption>
</figure>

<p>Then select <samp>Session</samp> from the category list and make the following changes:</p>
<dl>
	<dt>Host Name (or IP address)</dt>
	<dd><var>server</var>.feralhosting.com, where <var>server</var> is replaced by your actual server name</dd>
	<dt>Saved Sessions</dt>
	<dd>Enter a name of your choice to save the session under. Anything will do - why not your server name?</dd>
</dl>
<p>After making these changes click on <samp>Save</samp> to the right-hand side. All the changes you've made will be saved under the name specified in <samp>Saved Sessions</samp></p>
<figure>
    <img alt="Kitty screenshot showing a session being saved" src="https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/slots/ssh/ssh-session.png">
    <figcaption>Enter your details and then click <samp>Save</samp>.</figcaption>
</figure>

<p>If you <kbd>double-click</kbd> on the saved session name it will load up the session according to the settings you have provided. If you provided, as above, the username and password directly you'll be logged in without having to enter anything.</p>

<aside class="alert note">When you first connect you'll be met with a warning as in the image below. If you have set the details as described above it will be perfectly fine to press <kbd>Yes</kbd> to the message and cache the host key.</aside> 
<figure>
    <img alt="Image of a warning from Kitty. The text reads 'The server's host key is not cached in the registry. You
have no guarantee that the server is the computer you
think it is.'" src="https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/slots/ssh/ssh-hostkeywarn.png">
    <figcaption>If you've configured as above you can click <samp>Yes</samp>.</figcaption>
</figure>

<h2>Mac</h2>
<p>No special software is needed to access your slot via SSH if you use macOS. You can simply access the terminal and connect out of the box.</p>
<p>To access the terminal, access your <samp>Applications</samp> folder, then open <samp>Utilities</samp>. You'll see <samp>Terminal</samp> in the list. To save time in the future you can drag the icon for <samp>Terminal</samp> on to the Dock. </p>
<p>Once you have a terminal window open, you can run <kbd>ssh <var>user</var>@<var>server</var>.feralhosting.com</kbd>, where <var>user</var> is replaced by your username and <var>server</var> is replaced by the name of your server. You will then be prompted to enter your SSH password.</p>

<h2>Linux</h2>
<p>Firstly you need to access your terminal. Whilst there may obviously be differences between the different distributions of Linux, the standard way to access this in Debian, Ubuntu and Linux Mint is by pressing <kbd>ctrl</kbd> + <kbd>alt</kbd> + <kbd>T</kbd>.</p>
<p>Once you have a terminal window open, you can run <kbd>ssh <var>user</var>@<var>server</var>.feralhosting.com</kbd>, where <var>user</var> is replaced by your username and <var>server</var> is replaced by the name of your server. You will then be prompted to enter your SSH password.</p>
<p>If other flavours have different methods, feel free to write up subsections for them.</p>