#!/usr/bin/env bash
set -euo pipefail
#
# Copyright (c) 2017 Feral Hosting. This content may not be used elsewhere without explicit permission from Feral Hosting.
#
# This script can be used to perform common ruTorrent tasks - install plugins, update ruTorrent version, add users, restart and uninstall

# Functions

ruTorrentMenu ()
{
    echo -e "\033[36m""ruTorrent""\e[0m"
    if [[ ! -d ~/private/rtorrent/work ]] # check that user has (likely) installed rTorrent
    then
        echo -e "You haven't installed ruTorrent yet - please do so from the Feral software page: \nhttps://www.feralhosting.com/slots/$(hostname)/$(whoami)/software/"
        echo "Quitting the script..."
        echo
        exit
    else
        echo "1 Upgrade ruTorrent to latest version (3.8)"
        echo "2 Install plugins"
        echo "3 Change ruTorrent password"
        echo "4 Add ruTorrent user"
        echo "5 Restart rTorrent/ruTorrent"
        echo "6 Uninstall rTorrent/ruTorrent"
        echo "q Quit the script"
    fi
}

ruPlugMenu ()
{
    echo -e "\033[36m""ruTorrent Plugins""\e[0m"
    echo "1 Coloured ratio column"
    echo "2 Filemanager"
    echo "3 Fileshare"
    echo "4 Fileupload"
    echo "5 Mediastream"
    echo "6 Screenshots"
    echo "7 Unpack"
    echo "b Go back"
    echo "q Quit the script"
}

binCheck () # checks for ~/bin
{
    if [[ ! -d ~/bin ]]
    then
        echo "Creating ~/bin first"
        mkdir -p ~/bin
    fi
}

ffmpegInstall () # a function to be used by lots of other software
{
    binCheck
    echo "Downloading FFmpeg..."
    wget -qO ~/ffmpeg-temp.tar.gz https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-64bit-static.tar.xz
    echo "Extracting and configuring FFmpeg..."
    mkdir -p ~/ffmpeg-temp
    tar xf ~/ffmpeg-temp.tar.gz -C ~/ffmpeg-temp
    cp ~/ffmpeg-temp/ffmpeg-*-64bit-static/{ffmpeg,ffprobe,ffmpeg-10bit,qt-faststart} ~/bin
    rm -rf ~/ffmpeg-temp*
    echo "You have now installed $(~/bin/ffmpeg --version 2>&1 | head -n 1 | awk -F 'http' '{print $1}')"
    echo
}

filemanagerInstall () # a plugin for ruTorrent - used by many other ruTorrent plugins
{
    cd ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
    svn co -q https://github.com/nelu/rutorrent-thirdparty-plugins/trunk/filemanager && cd
}

while [ 1 ]
do
    echo
    ruTorrentMenu
    echo
    read -ep "Enter the number of the option you want: " CHOICE
    echo
    case "$CHOICE" in
        "1") # install latest version of ruTorrent
            echo -e "Though torrent files and data will be untouched," "\033[31m""ruTorrent settings and custom plugins will be removed""\e[0m"
            read -ep "Are you sure you want to upgrade? [y] yes or [n] no: " CONFIRM
            if [[ $CONFIRM =~ ^[Yy]$ ]]
            then
                echo "Upgrading ruTorrent"
                cd ~/www/$(whoami).$(hostname -f)/public_html
                git clone https://github.com/Novik/ruTorrent.git > /dev/null 2>&1
                cp -r rutorrent/conf/* ruTorrent/conf/
                cp rutorrent/.ht* ruTorrent/
                rm -rf rutorrent/ && mv ruTorrent/ rutorrent/
                rm -rf rutorrent/plugins/{cpuload,diskspace,unpack}
                echo "ruTorrent has been upgraded to the latest version."
                echo
                fi
                ;;
        "2") # rutorrent plugins
            while [ 1 ]
            do
                ruPlugMenu
                echo
                read -ep "Enter the number of the plugin you want to install: " CHOICE
                echo
                case "$CHOICE" in
                    "1") # coloured ratio columns
                        echo "Getting and installing the coloured ratio columns plugin..."
                        wget -qO ~/ratio.zip http://git.io/71cumA
                        unzip -qo ~/ratio.zip -d ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                        rm -f ratio.zip
                        echo "The coloured ratio columns plugin has been installed. Refresh ruTorrent if it's open to see the changes."
                        echo
                        ;;
                    "2") # filemanager
                        echo "Installing Filemanager..."
                        filemanagerInstall
                        if [[ ! -f ~/bin/ffmpeg ]]
                        then
                            echo "Installing FFmpeg for the screenshots function..."
                            ffmpegInstall
                        fi
                        echo "Configuring Filemanager to work with your FFmpeg..."
                        sed -i "s|(getExternal(\"ffprobe\")|(getExternal(\"~/bin/ffprobe\")|g" ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/filemanager/flm.class.php
                        sed -i "s|(getExternal('ffmpeg')|(getExternal('$(pwd)/bin/ffmpeg')|g" ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/filemanager/flm.class.php
                        echo "Filemanager has been installed and configured. Refresh ruTorrent if it's open to see the changes."
                        echo
                        ;;
                    "3") # fileshare
                        if [[ ! -d ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/filemanager ]]
                        then
                            echo "Installing Filemanager first"
                            filemanagerInstall
                        fi
                        echo "Installing Fileshare"
                        cd ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                        svn co -q https://github.com/nelu/rutorrent-thirdparty-plugins/trunk/fileshare && cd
                        echo "Configuring Fileshare..."
                        ln -s ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/fileshare/share.php ~/www/$(whoami).$(hostname -f)/public_html/
                        sed "/if(getConfFile(/d" -i ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/fileshare/share.php
                        sed -i "s|'http://mydomain.com/share.php';|'http://$(whoami).$(hostname -f)/share.php';|g" ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/fileshare/conf.php
                        echo "Fileshare has been installed and configured. Refresh ruTorrent if it's open to see the changes."
                        echo
                        ;;
                    "4") # fileupload
                        binCheck
                        echo "Getting and installing Plowshare..."
                        git clone https://github.com/mcrapet/plowshare.git ~/.plowshare-source > /dev/null 2>&1
                        cd ~/.plowshare-source
                        make install PREFIX=$HOME > /dev/null 2>&1
                        cd && rm -rf ~/.plowshare-source
                        echo "Configuring Plowshare..."
                        ~/bin/plowmod --install
                        echo "Installing the Fileupload plugin..."
                        cd ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                        svn co -q https://github.com/nelu/rutorrent-thirdparty-plugins/trunk/fileupload && cd
                        echo "Fileupload has been installed and configured. Refresh ruTorrent if it's open to see the changes."
                        echo
                        ;;
                    "5") # mediastream
                        if [[ ! -d ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/filemanager ]]
                        then
                            echo "Installing Filemanager first"
                            filemanagerInstall
                        fi
                        echo "Installing Mediastream..."
                        cd ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                        svn co -q https://github.com/nelu/rutorrent-thirdparty-plugins/trunk/mediastream && cd
                        echo "Configuring Mediastream..."
                        mkdir -p ~/www/$(whoami).$(hostname -f)/public_html/stream
                        ln -s ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/mediastream/view.php ~/www/$(whoami).$(hostname -f)/public_html/stream/
                        sed -i "s|'http://mydomain.com/stream/view.php';|'http://$(whoami).$(hostname -f)/stream/view.php';|g" ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/mediastream/conf.php
                        echo "Mediastream has been installed and configured. Refresh ruTorrent if it's open to see the changes."
                        echo
                        ;;
                    "6") # screenshots
                        if [[ ! -f ~/bin/ffmpeg ]]
                        then
                            echo "Installing FFmpeg first..."
                            ffmpegInstall
                        fi
                        if grep -q 3.8 ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/_getdir/plugin.info # check if version 3.8 from must-have plugin
                        then
                            echo "Installing the 3.8 version of the plugin"
                            rm -rf ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/screenshots
                            cd ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                            svn co -q https://github.com/Novik/ruTorrent/trunk/plugins/screenshots && cd
                        else
                            echo "Installing the 3.6 version of the plugin"
                            rm -rf ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/screenshots
                            wget -q https://bintray.com/artifact/download/novik65/generic/plugins/screenshots-3.6.tar.gz
                            tar xf screenshots-3.6.tar.gz -C ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                            rm screenshots-3.6.tar.gz
                        fi
                        echo "Configuring screenshots to work with your FFmpeg"
                        cd && sed -i "s|ffmpeg'] = ''|ffmpeg'] = '$(pwd)/bin/ffmpeg'|g" ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/screenshots/conf.php
                        echo "Screenshots has been installed and configured. Refresh ruTorrent if it's open to see the changes."
                        echo
                        ;;
                    "7") # unpack
                        if grep -q 3.8 ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/_getdir/plugin.info # check if version 3.8 from must-have plugin
                        then
                            echo "Installing the 3.8 version of the plugin"
                            rm -rf ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/unpack
                            cd ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                            svn co -q https://github.com/Novik/ruTorrent/trunk/plugins/unpack && cd
                        else
                            echo "Installing the 3.6 version of the plugin"
                            rm -rf ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/unpack
                            wget -q https://bintray.com/artifact/download/novik65/generic/plugins/unpack-3.6.tar.gz
                            tar xf unpack-3.6.tar.gz -C ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/plugins/
                            rm unpack-3.6.tar.gz
                        fi
                            echo "Unpack has been installed and configured. Refresh ruTorrent if it's open to see the changes."
                            echo
                            ;;
                        "b") # go back to main menu
                            break
                            ;;
                        "q") # quit
                            exit
                            ;;
                esac
            done
            ;;
        "3") # change ruTorrent password
            read -ep "For which username do you want to change the ruTorrent password?: " USERNAME
            htpasswd -m ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/.htpasswd $USERNAME
            echo
            ;;
        "4") # add ruTorrent user
            read -ep "Which username to you wish to add to ruTorrent?: " USERNAME
            htpasswd -m ~/www/$(whoami).$(hostname -f)/public_html/rutorrent/.htpasswd $USERNAME
            echo
            ;;
        "5") # restarting rTorrent
            pkill -fu "$(whoami)" 'rtorrent' || true
            sleep 3 && screen -dmS rtorrent rtorrent
            echo "rTorrent has been restarted"
            echo
            ;;
        "6") # uninstall ruTorrent
            echo -e "Uninstalling ruTorrent will" "\033[31m""remove rTorrent, ruTorrent, the associated torrents, their data and the software settings""\e[0m"
            read -ep "Are you sure you want to uninstall? [y] yes or [n] no: " CONFIRM
            if [[ $CONFIRM =~ ^[Yy]$ ]]
            then
                pkill -9 -fu "$(whoami)" 'rtorrent'
                rm -rf ~/.rtorrent.rc ~/private/rtorrent ~/www/$(whoami).$(hostname -f)/public_html/rutorrent
                echo
                echo "ruTorrent and rTorrent have now been removed. Their entries will still appear on your Feral software page but the details will not work."
                echo
                break
            else
                echo "Taking no action..."
                echo
            fi
            ;;
        "q") # quit the script
            exit
            ;;
    esac
done